#!/bin/bash

source .env
source ./doc/usage.sh
set -e # exit on error

case $1 in
#### DOCKER COMMANDS 32
  up)
    #h# Create and start the container
    docker-compose -p ${projectName} -f ./docker/docker-compose.yml up -d
    ;;
  start)
    #h# Start the container
    docker-compose -f ./docker/docker-compose.yml -p ${projectName} start
    ;;
  stop)
    #h# Stop the container
    docker-compose -f ./docker/docker-compose.yml -p ${projectName} stop
    ;;
  down)
    #h# Stop and remove the container
    docker-compose -f ./docker/docker-compose.yml -p ${projectName} down
    ;;
  run)
    #h# Run a shell command in the container
    shift
    docker exec -it "${containerName}" $*
    ;;
  docker-config)
    #h# Get the configuration of docker-compose (using the .env file)
    docker-compose -f ./docker/docker-compose.yml config
    ;;
  connect)
    #h# Connect to the container
    _color 31 "to exit run Ctrl + q Ctrl + q"
    docker exec -it "${containerName}" /bin/bash ;; #TODO container name and handle bash path
#### Python commands 95
  py)
    #h# Acces python interpretor or run a python script
    #o# [script.py]
    shift
    docker exec -it "${containerName}" python $*
    ;;
  version)
    #h# Get the version of python on the container
    docker exec -it "${containerName}" python -V
    ;;
#### HELP 31
  --help|-h|help|usage|h)
    #h# display help
    usage ;;
  *) _color 31 "The super ./run.sh cannot understand what you said, sorry :( "
    echo "see './run.sh help' "
esac
