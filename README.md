# Python dev environment in Docker

## Launch, stop and remove the container

start the container: `./run.sh up `  
stop the container: `./run.sh stop`  
stop and remove the container: `/run.sh down`  
check the version of python: `./run.sh version `

## How to use it ?
Use the python interpreter: `/run.sh py`  
Exit with `exit()`  

Run a python script: `/run.sh py [scriptpPath]`  

You can also run bash command inside the container: `./run.sh run echo "i'm inside !" ` 

### Use a different version, change names ..
You can change the version oh python with the variable TAG in .env file. 
Choose a tag according to yours needs on the [dockerhub](https://hub.docker.com/_/python)

You can change the name of the stack and the container by changing *projectName* and *containerName*.  
### Useful aliases
Copy paste the following lines in your *bashrc* file or *aliases* file:
```
alias run="./run.sh"
alias py="run py"
```
You can now use `run` instead of `./run.sh` and `py` instead of `.run.sh py`
Of course, you can replace `run` or `py` by a preferred alias name.
 
emile_ch


